# Remote Soil Moisture Sensor PCB

## Interactive BOM

[Interactive BOM](https://nf3rn0.gitlab.io/ms_xbee_pcb/)

## Features

* Digi Xbee 3 Pro Module
* Analog Input for Capactive Soil Moisture Sensor
* Powered by Single 18650
* Solar Charging
* USB-C Charging
* Battery Protection

## Design

Designed using Kicad and other open source design tools.

Footprints used in this source contain Creative Commons Attribution-ShareAlike 4.0 components from Digi-Key Electronics and SnapEDA.

A fully functional design was fabricated using OSH Park.

## License Notice

Copyright Dustin Martin 2022.

This source describes Open Hardware and is licensed under the CERN-OHL-W v2 or later

You may redistribute and modify this documentation and make products
using it under the terms of the CERN-OHL-W v2 (https:/cern.ch/cern-ohl).
This documentation is distributed WITHOUT ANY EXPRESS OR IMPLIED
WARRANTY, INCLUDING OF MERCHANTABILITY, SATISFACTORY QUALITY
AND FITNESS FOR A PARTICULAR PURPOSE. Please see the CERN-OHL-W v2
for applicable conditions.

Source location: https://gitlab.com/NF3RN0/ms_xbee_pcb

As per CERN-OHL-W v2 section 4.1, should You produce hardware based on
these sources, You must maintain the Source Location visible on the
external case of the Remote Soil Moisture Sensor PCB or other product you make using this documentation.